package Game;

/**
 * Text based fighting simulator External libraries used: TextIO
 * (http://math.hws.edu/javanotes/source/chapter4/TextIO.java)
 * 
 * @author margit kont
 *
 */
public class Base {

	static Player hero;

	/**
	 * Class constructor
	 */
	public Base() {
		hero = new Player();
	}

	/**
	 * Game begins, new instance for game is created. Hero gets a name, combat
	 * begins. + flavour text
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Base game = new Base();

		System.out.println("Greetings, traveler!");
		System.out.println("What is your name? ");
		System.out.println("Enter your name: ");
		hero.player_name();
		System.out.println("Well, " + hero.name + ", these lands are full of danger,");
		System.out.println("be aware and try not to get in trouble ");
		game.combat();
	}

	/**
	 * Heart of the game- combat. Monster is created, given a name and fighting
	 * starts. If player dies, game exits. If Monster dies, player is given
	 * experience and may randomly find loot
	 */
	public void combat() {
		Monster baddie = new Monster();
		baddie.monsterName();
		System.out.println("A monster named " + baddie.name + " is in your way. You must fight it: ");
		while ((!hero.dead && !baddie.dead) == true) {
			boolean heal_flag = false;
			if (hero.creatureDeath()) {
				System.out.println("You died. Game over");
				System.exit(0);
			}

			boolean done = false;
			while (!done) {

				hero.damage = hero.user_action();
				baddie.hp -= hero.damage;
				if (baddie.hp < 0)
					baddie.hp = 0;
				if (hero.damage == 0) {
					heal_flag = hero.heal();
					if (!heal_flag) {
						break;
					}
				}
				if (!heal_flag) {
					System.out.println("You attacked the monster and did " + hero.damage + " damage.\n" + baddie.name
							+ " now has " + baddie.hp + " health left.");
				}
				if (baddie.creatureDeath()) {
					hero.player_experience();
					System.out.println(
							baddie.name + " Has died. You have gained 25 experience,\n and now have a total of "
									+ hero.xp + " experience points.");
					baddie = null;
					loot();
					}
					combat();
				}
				baddie.damage = baddie.damage();
				hero.hp -= baddie.damage;
				System.out.println(baddie.name + " attacked you and did " + baddie.damage + " damage. You have now "
						+ hero.hp + " health left.");
				done = true;

			}

		}
	/**
	 * 	May give Player loot, depending on a random roll.
	 */
	public void loot(){
		int randomNum = (int) (Math.random() * 6);
		if (randomNum == 3) {
			randomNum = (int) (Math.random() * 6);
			System.out.println(randomNum);
			if (randomNum == 5) {
				System.out.println("you found a better sword, and it increased your damage! \n You now do "
						+ hero.dmg_min + "-" + hero.dmg_max + " damage");
				hero.dmg_min += 10;
				hero.dmg_max += 10;
			} else if (randomNum == 1 || randomNum == 2) {
				System.out.println(
						"you found some food, and gained 20 hp! \n You have now " + hero.hp + " hp.");
				hero.hp += 20;
			} else {
				hero.mana += 20;
				System.out.println("you found some water, and gained 20 mana! \n You have now " + hero.mana
						+ " mana.");
			}
		}

	}

}
