package Game;

import lib.TextIO;

/**
 * 	Player is extension of creature, gaining a few additional variables and methods.
 * 
 * @author margit kont
 *
 */
public class Player extends Creature {

	String name;
	int xp = 0;
	int xp_req = 25;
	int mana = 100;

	
	/**
	 * 	Player can give name to hero
	 */
	public void player_name() {
		name = TextIO.getln();

	}

	/**
	 * 	Heals the player, using mana.
	 * @return boolean to combat, checking if the player was able to heal or not. 
	 */
	public boolean heal() {
		if (mana >= 20) {
			hp += 30;
			if (hp > hp_max)
				hp = hp_max;

			mana -= 20;
			System.out.println(" You healed yourself 30 hp, you have now " + hp + " health left.\n You have " + mana
					+ " mana left.");
			return true;
		} else {
			System.out.println("You don't have enough mana");
			return false;
		}
	}

	/**
	 * Adds experience, if player has enough xp, levels up and adds to stats.
	 */
	public void player_experience() {
		xp += 25;

		if (xp == xp_req) {
			levelUp();
			System.out.println("You leveled up! You are now level " + level);
			System.out.println(" and you have now " + hp + " health and do " + dmg_min + "-" + dmg_max + " damage");
			xp = 0;
			xp_req += 25;
			mana += 20;
		}
	}

	/**
	 * @return int damage, to do damage in combat, in case of healing do 0 damage.
	 */
	public int user_action() {
		String action = "";
		while (!action.equalsIgnoreCase("attack") & !action.equalsIgnoreCase("heal")) {
			System.out.println("Enter your action \n[ATTACK] [HEAL]");
			action = TextIO.getln();
		}
		if (action.equalsIgnoreCase("attack")) {
			int damage = damage();

			return damage;
		}
		if (action.equalsIgnoreCase("heal")) {
			int damage = 0;
			return damage;
		}
		return 0;
	}

}
