package Game;

/**
 * 	Creature is a base with variables and methods for both Player and Monster classes.
 * 
 * @author margit kont
 *
 */
public class Creature {

	int dmg_min = 10;
	int dmg_max = 20;
	int hp = 100;
	int hp_max = 100;
	int level = 1;
	int damage = 0;
	boolean dead = false;

	/**
	 * @return boolean dead, if true then creature is terminated in Base.
	 */
	public boolean creatureDeath() {
		if (hp <= 0) {
			dead = true;
		}
		return dead;

	}

	/**
	 * 	Generates random damage value using Math.Random().
	 * 
	 * @return int dmg value to subtract from hp in base.
	 */
	public int damage() {
		int range = (dmg_max - dmg_min) + 1;
		int dmg = (int) (Math.random() * range) + dmg_min;
		return dmg;
	}

	/**
	 * 	Levels the creature up, adding to stats.
	 */
	public void levelUp() {
		level++;
		hp_max += 15;
		dmg_min += 5;
		dmg_max += 5;
		hp = hp_max;
	}

}
