package Game;

/**
 * 	Monster is an extension of Creature, with a randomly chosen name.
 * 
 * @author margit kont
 *
 */
public class Monster extends Creature {

	public String name;

	/**
	 * 	Selects a random name for the monster from an array of names.
	 */
	public void monsterName() {
		String[] names = { "Clark", "Lora", "Shaquita", "Denae", "Zulma", "Halina", "Candis", "Omer", "Zachariah",
				"Kristen", "Talia", "Coy", "Naomi", "Erline", "Darren", "Tiara", "Miles", "Diann", "Cari", "Johnnie",
				"Serafina", "Romeo", "Phil", "Jayna", "Carrol", "Jada", "Deneen", "Gayle", "Julianna", "Carissa",
				"Kitty", "Alysia", "Jonah", "Cristobal", "Linsey", "Perla", "Cherelle", "Nedra", "Edythe", "Kiara",
				"Teodora", "Zenobia", "Bernadine", "Quinton", "Josefina", "Lula", "Belle", "Dusty", "Keva", "Vickie" };
		int randomNum = (int) (Math.random() * 50);
		name = names[randomNum];
	}

}
